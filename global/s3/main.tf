provider "aws" {
  region = "eu-west-2"
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "damo-terraform-up-and-running-state"

  #prevent accidential deletion of this S3 bucket
  lifecycle {
      create_before_destroy = true
  }
  
  #enable versioning of files
  versioning {
      enabled = true
  }

  #enable server side encryption by default
  server_side_encryption_configuration {
      rule {
          apply_server_side_encryption_by_default {
              sse_algorithm = "AES256"
          }
      }
  }
}

resource "aws_dynamodb_table" "terraform_locks" {
  name          = "tf-dynamodb_table-locks"
  billing_mode  = "PAY_PER_REQUEST"
  hash_key      = "LockID"

  attribute {
      name = "LockID"
      type = "S"
  }
}


# Partial configuration. The other settings (e.g., bucket, region) will be
# passed in from a backend.hcl file via -backend-config arguments to 'terraform init'
terraform {
    backend "s3" {
        key         = "global/s3/terraform.tfstate"

    }
}