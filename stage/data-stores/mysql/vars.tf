variable "db_password" {
  type          = string
  description   = "The password for the database"
}

variable "db_name" {
  description   = "Name of the database"
  type          = string
  default       = "stage"

}