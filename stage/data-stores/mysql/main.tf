terraform {
    backend "s3" {
        key = "stage/data-stores/mysql/terraform.tfstate"
    }
}

provider "aws" {
    region = "eu-west-2"
}

module "database" {
  source = "git::https://gitlab.com/krox/terraform-modules.git//data-stores/mysql?ref=v0.1.0"
  #A special double-slash syntax is interpreted by Terraform to indicate that the remaining path after that point is a sub-directory within the package 
  
  db_name = var.db_name
  db_password   = var.db_password
}