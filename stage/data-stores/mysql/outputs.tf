output "db_address" {
    value       = module.database.db_address
    description = "Database address"
}

output "port" {
    value       = module.database.port
    description = "Database port"
}