terraform {
    backend "s3" {
        key         = "prod/services/webserver-cluster/terraform.tfstate"

    }
}

provider "aws" {
  region = "eu-west-2"
}



module "webserver-cluster" {
  source = "git::https://gitlab.com/krox/terraform-modules.git//services/webserver-cluster?ref=v0.1.0"
  #A special double-slash syntax is interpreted by Terraform to indicate that the remaining path after that point is a sub-directory within the package
  
  cluster_name            = "prod"
  db_remote_state_bucket  = "damo-terraform-up-and-running-state"
  db_remote_state_key     = "prod/data-stores/mysql/terraform.tfstate"
}